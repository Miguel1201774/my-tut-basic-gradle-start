package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void createValidEmployee(){
        //Arrange
        String firstName="Miguel";
        String lastName="Oliveira";
        String description = "Student";
        String emailAddress = "1201774@isep.ipp.pt";
        //Act
        Employee miguel=new Employee(firstName,lastName,description,emailAddress);
        //Assert
        assertNotNull(miguel);
    }


    @Test
    void createOtherValidEmployee(){
        //Arrange
        String firstName="Frodo";
        String lastName="Baggins";
        String description="ring bearer";
        String emailAddress="frodo@hotmail.com";
        //Act
        Employee miguel=new Employee(firstName,lastName,description,emailAddress);
        //Assert
        assertNotNull(miguel);
    }

    @Test
    void createEmployeeEmptyFirstName(){
        //Arrange
        String firstName="";
        String lastName="Oliveira";
        String description="Student";
        String emailAddress="1201774@isep.ipp.pt";
        //Act & Assert
        assertThrows(IllegalArgumentException.class,()->{new Employee(firstName,lastName,description,emailAddress);});
    }

    @Test
    void createEmployeeNullLastName(){
        //Arrange
        String firstName="Miguel";
        String lastName=null;
        String description="Student";
        String emailAddress="1201774@isep.ipp.pt";
        //Act & Assert
        assertThrows(IllegalArgumentException.class,()->{new Employee(firstName,lastName,description,emailAddress);});
    }

    @Test
    void createEmployeeEmptyDescription(){
        //Arrange
        String firstName="Miguel";
        String lastName="Oliveira";
        String description="";
        String emailAddress="1201774@isep.ipp.pt";
        //Act & Assert
        assertThrows(IllegalArgumentException.class,()->{new Employee(firstName,lastName,description,emailAddress);});
    }

    @Test
    void createEmployeeEmptyEmailAddress(){
        //Arrange
        String firstName="";
        String lastName="Oliveira";
        String description="Student";
        String emailAddress="";
        //Act & Assert
        assertThrows(IllegalArgumentException.class,()->{new Employee(firstName,lastName,description,emailAddress);});
    }

    @Test
    void createEmployeeInvalidEmail(){
        //Arrange
        String firstName="Miguel";
        String lastName="Oliveira";
        String description="Student";
        String emailAddress="1201774.isep.ipp.pt";
        //Act & Assert
        assertThrows(IllegalArgumentException.class,()->{new Employee(firstName,lastName,description,emailAddress);});
    }

    @Test
    void createEmployeeInvalidEmailTwo(){
        //Arrange
        String firstName="Miguel";
        String lastName="Oliveira";
        String description="Student";
        String emailAddress="1201774@isep@ipp.pt";
        //Act & Assert
        assertThrows(IllegalArgumentException.class,()->{new Employee(firstName,lastName,description,emailAddress);});
    }

    @Test
    void createEmployeeInvalidEmailThree(){
        //Arrange
        String firstName="Miguel";
        String lastName="Oliveira";
        String description="Student";
        String emailAddress="@isep.ipp.pt";
        //Act & Assert
        assertThrows(IllegalArgumentException.class,()->{new Employee(firstName,lastName,description,emailAddress);});
    }

    @Test
    void createEmployeeInvalidEmailFive(){
        //Arrange
        String firstName="Miguel";
        String lastName="Oliveira";
        String description="Student";
        String emailAddress="1201774@";
        //Act & Assert
        assertThrows(IllegalArgumentException.class,()->{new Employee(firstName,lastName,description,emailAddress);});
    }

}